import React from 'react'
import ReactDOM from 'react-dom'
import { MuiPickersUtilsProvider } from '@material-ui/pickers'
import DateFnsUtils from '@date-io/date-fns'

import './index.css'
import "antd/dist/antd.css"
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css"
import 'react-toastify/dist/ReactToastify.css'

import Root from 'containers/Root'
import * as serviceWorker from './serviceWorker'

import { FavoritesProvider } from 'providers/Favorites'
import { StashProvider } from 'providers/Stash'
import { GadgetStashProvider } from 'providers/GadgetStash'
import { SettingsProvider } from 'providers/Settings'
import { TicketsProvider } from 'providers/Tickets'

const ProviderParent = () => {
    return (
        <MuiPickersUtilsProvider utils={DateFnsUtils}>
            <SettingsProvider>
                <FavoritesProvider>
                    <GadgetStashProvider>
                        <StashProvider>
                            <TicketsProvider>
                                <Root />
                            </TicketsProvider>
                        </StashProvider>
                    </GadgetStashProvider>
                </FavoritesProvider>
            </SettingsProvider>
        </MuiPickersUtilsProvider>
    )
}
ReactDOM.render(<ProviderParent />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
