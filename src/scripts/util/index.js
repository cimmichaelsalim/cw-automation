export const getUserName = () => {
    return fetch('https://eu.myconnectwise.net/v2020_2/login/IsAuthenticated.aspx', {
        credentials: 'include',
        headers: {
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:68.0) Gecko/20100101 Firefox/68.0',
            Accept: '*/*',
            'Accept-Language': 'en-US,en;q=0.5',
            'cache-control': 'no-cache',
            'Content-Type': 'text/plain;charset=UTF-8',
        },
        referrer: 'https://eu.myconnectwise.net/v2020_2/ConnectWise.aspx?locale=en_US&session=new',
        method: 'POST',
        mode: 'cors',
    })
        .then(res => res.json())
        .then(json => json.memberId)
}
