import axios from 'axios'

import pageApi from './pageApi'

import { formatCWTime } from 'helper'

export default (credentials: any) => async (date: Date, timeStart: Date, timeEnd: Date, serviceRecID: number, notes: string, overrideActualHours: number | null) => {
    if (credentials && credentials.enableApi) {
        const payload = {
            dateEntered: formatCWTime(date),
            chargeToId: serviceRecID,
            chargeToType: 'ProjectTicket',
            ...(!!overrideActualHours ? {} : { timeStart: formatCWTime(timeStart) }),
            ...(!!overrideActualHours ? {} : { timeEnd: formatCWTime(timeEnd) }),
            actualHours: !!overrideActualHours ? overrideActualHours : (timeEnd.getTime() - timeStart.getTime()) / 60000 / 60,
            notes: notes,
            addToDetailDescriptionFlag: false,
            addToInternalAnalysisFlag: false,
            addToResolutionFlag: false
        }
        
        return new Promise((resolve, reject) => {
            createTimEntryWithAPI(credentials, payload).then(res => {
                resolve(res.status === 201)
            }).catch(err => {
                reject(err)
            })
        })
    }else{
        return pageApi(date, timeStart, timeEnd, serviceRecID, notes, overrideActualHours)
    }
}

const createTimEntryWithAPI = ({ publicKey, privateKey, clientID }: any, payload: any) => {
	console.log("Creating time entry with following payload: ", payload)
	return axios.post(`https://api-eu.myconnectwise.net/v2020_2/apis/3.0/time/entries`, payload, {
		headers: {
			'Accept': `application/vnd.connectwise.com+json; version=2019.5, application/json`,
			'Authorization': `Basic ${Buffer.from(`cimlogic+${publicKey}:${privateKey}`).toString('base64')}`,
			'Cache-Control': 'no-cache',
			'clientid': clientID
		},
	})
}