export default userName => {
	return fetch(
		'https://eu.myconnectwise.net/v2020_2/services/system_io/actionprocessor/System/GetMemberWithSecurityAction.rails',
		{
			credentials: 'include',
			headers: {
				'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:68.0) Gecko/20100101 Firefox/68.0',
				Accept: '*/*',
				'Accept-Language': 'en-US,en;q=0.5',
				'Content-Type': 'application/x-www-form-urlencoded',
				'cache-control': 'no-cache',
				'X-Amzn-Trace-Id': 'B01A136D-A923-423A-926C-F6FDCD6891CC',
				'cw-app-id': 'bm-manageclient',
				clientId: 'b495a7ce-5b88-42ba-ac2a-621d61b53131',
			},
			referrer: 'https://eu.myconnectwise.net/v2020_2/ConnectWise.aspx?locale=en_US&session=new',
			body: `actionMessage=%7B%22payload%22%3A%22%7B%5C%22memberId%5C%22%3A%5C%22${userName}%5C%22%7D%22%2C%22payloadClassName%22%3A%22GetMemberWithSecurityAction%22%2C%22project%22%3A%22SystemCommon%22%7D&clientTimezoneOffset=60&clientTimezoneName=GMT+Standard+Time`,
			method: 'POST',
			mode: 'cors',
		},
	)
		.then(res => res.json())
		.then(json => json.data.action.member.recID)
}
