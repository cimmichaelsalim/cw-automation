import getDefaults from './getDefaults'
import getMemberRecID from './getMemberRecID'

import { getUserName } from 'scripts/util'

export default async (date: Date, timeStart: Date, timeEnd: Date, serviceRecID: number, notes: string, overrideActualHours: number | null) => {
    const userName = await getUserName()
    const data: any = await getDefaults(userName, serviceRecID)

    const companyId = data.cmp_rid
    const billableOptionsRecID = data.bill_rid
    const activityClassName = data.wr_name
    const activityClassRecID = data.wr_rid
    const activityTypeName = data.wt_name
    const activityTypeRecID = data.wt_rid
    const contactRecID = data.contact_rid
    const billingUnitRecId = data.bu_rid
    const ownerLevelRecId = data.ol_rid
    const ticketStatusRecId = data.sr_status_id

    const memberRecID = await getMemberRecID(userName)

    const projectRID = parseInt(data.pm_rid, 10)
    const serviceRID = parseInt(data.sr_rid, 10)

    const payload = {
        timeEntryModel: {
            isEnteringTime: true,
            TE_Problem_Flag: true,
            company_RecID: companyId,
            contact_RecID: contactRecID, // Notes contact id
            member_RecID: memberRecID, // Default, own id
            PM_Project_RecID: projectRID, // To get
            SR_Service_RecID: serviceRID, // To get
            popOutHeight: 966, // -
            popOutWidth: 1332, // -
            billing_Unit_RecID: billingUnitRecId, // GROUP
            owner_Level_RecID: ownerLevelRecId, // GROUP
            hourly_Rate: 0, // Self explanitory
            hours_Actual: !!overrideActualHours ? overrideActualHours : (timeEnd.getTime() - timeStart.getTime()) / 60000 / 60, // Self explanitory
            hours_Bill: 0, // Self explanitory
            hours_Deduct: 0, // Self explanitory
            hours_Invoiced: 0, // Self explanitory
            billable_Options_RecID: billableOptionsRecID, // Do not bill
            ticketStatusRecId, // Ticket status: new
            activityClassName,
            activity_Class_RecID: activityClassRecID, // Graduate
            activityTypeName,
            activity_Type_RecID: activityTypeRecID, // Hourly rate
            expenseDocumentGuid: '0b44c6bc-5992-4bb1-ad1e-ebfb5a8182ef',
            from: 'donotreply@cimlogic.co.uk',
            member_ID: userName,
            notes,
            date_Start: date.getTime().toString(),
            lastLoadTime: new Date().getTime().toString(),
            scheduleDate: new Date().getTime().toString(),
            ...(!!overrideActualHours ? {} : { time_End: timeEnd.getTime().toString() }),
            ...(!!overrideActualHours ? {} : { time_Start: timeStart.getTime().toString() }),
            documentRecIdList: [],
        },
    }
    const payloadString = JSON.stringify(payload)

    const actionMessageObject = {
        payload: `${payloadString}`,
        payloadClassName: 'UpdateTimeEntryAction',
        project: 'TimeCommon',
    }

    const actionMessage = encodeURIComponent(JSON.stringify(actionMessageObject))

    const body = `actionMessage=${actionMessage}&clientTimezoneOffset=60&clientTimezoneName=GMT+Standard+Time`

    const res = await fetch(
        'https://eu.myconnectwise.net/v2020_2/services/system_io/actionprocessor/Time/UpdateTimeEntryAction.rails',
        {
            credentials: 'include',
            headers: {
                'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:68.0) Gecko/20100101 Firefox/68.0',
                Accept: '*/*',
                'Accept-Language': 'en-US,en;q=0.5',
                'Content-Type': 'application/x-www-form-urlencoded',
                'cache-control': 'no-cache',
                'cw-app-id': 'bm-manageclient',
                clientId: 'b495a7ce-5b88-42ba-ac2a-621d61b53131',
            },
            referrer: 'https://eu.myconnectwise.net/v2020_2/ConnectWise.aspx?locale=en_US&session=new',
            body: body,
            method: 'POST',
            mode: 'cors',
        },
    )

    return res
}