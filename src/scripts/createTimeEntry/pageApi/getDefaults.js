export default (userName, serviceRecID) =>
	fetch(
		`https://eu.myconnectwise.net/v2020_2/services/system_io/TimeEntry/ChargeToTree.aspx?method=picksr&entityOwnerID=${userName}&serviceRecID=${serviceRecID}`,
		{
			credentials: 'include',
			headers: {
				'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:68.0) Gecko/20100101 Firefox/68.0',
				Accept: '*/*',
				'Accept-Language': 'en-US,en;q=0.5',
				'Content-Type': 'text/plain; charset=utf-8',
				Pragma: 'no-cache',
				'Cache-Control': 'no-cache',
			},
			method: 'GET',
			mode: 'cors',
		},
	)
		.then(res => res.text())
		.then(async text => {
			const match = text.match(/<data (.+)\/>/)

			const data = match[1]
				.split('" ')
				.filter(val => val !== '')
				.map(line => {
					const fullLine = line + '"'
					const splitted = fullLine.split('=')
					return splitted
				})
				.reduce((acc, val) => {
					acc[val[0]] = val[1].match(/"(.+)?"/)[1]
					return acc
				}, {})

			return data
		})
