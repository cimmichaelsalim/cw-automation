import axios from 'axios'

export default (credentials) => async userName => {
	if (credentials) {
		let responseLength = 1000
		let count = 1
		let tickets = []
		while (responseLength === 1000) {
			const response = await getTicketsWithAPI(credentials, count)

			responseLength = response.data.length
			count++
			tickets = [...tickets, ...response.data]
		}
		return tickets
	} else {
		return fetch(
			`https://eu.myconnectwise.net/v2020_2/time/te500/getchargetotree.rails?overrideMemberID=${userName}&selectedCompanyRecIds=0&filter=&chargeToType=time`,
			{
				credentials: 'include',
				headers: {
					'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:68.0) Gecko/20100101 Firefox/68.0',
					Accept: '*/*',
					'Accept-Language': 'en-US,en;q=0.5',
					'Content-Type': 'text/plain; charset=utf-8',
				},
				method: 'GET',
				mode: 'cors',
			},
		)
			.then(res => res.json())
			.then(json => {
				// const companyId = json.companies[0].id
				// const projectsTree = json.companies[0].projects
				// copy(JSON.stringify({ companyId, projectsTree }))
				// console.log("Result Copied!")
			})

	}
}

const getTicketsWithAPI = ({ publicKey, privateKey, clientID }, page) => {
	return axios.get(`https://api-eu.myconnectwise.net/v2020_2/apis/3.0/project/tickets?pageSize=1000&page=${page}`, {
		headers: {
			'Accept': `application/vnd.connectwise.com+json; version=2019.5, application/json`,
			'Authorization': `Basic ${Buffer.from(`cimlogic+${publicKey}:${privateKey}`).toString('base64')}`,
			'Cache-Control': 'no-cache',
			'clientid': clientID
		},
	})
}