import React, { useState } from 'react'
import { Button, TextField } from '@material-ui/core'
import { KeyboardDatePicker, KeyboardTimePicker } from '@material-ui/pickers'
import { TreeSelect } from 'antd';

import useConnectWiseCheck from 'hooks/useConnectWiseCheck'
import { useStashes } from 'providers/Stash'
import { useTickets } from 'providers/Tickets';
import { useSettings } from 'providers/Settings';

import styles from './index.module.css'

export default (props) => {
    const { settings } = useSettings()
    const { loading, onConnectWise } = useConnectWiseCheck()
    const { addStash } = useStashes()
    const { tickets } = useTickets()

    const [submitting, setSubmitting] = useState(false)

    const {
        date, setDate, timeStart, setTimeStart, timeEnd, setTimeEnd, serviceRID, setServiceRID,
        notesEnabled,
        notes, setNotes,
        useSimpleTime,
        actualHours, setActualHours,
        onRun,
        showStash = true,
    } = props

    return (
        <>
            <div className={styles.formContainer}>
                <div className={styles.formRow}>
                    <KeyboardDatePicker label="Date" value={date} onChange={date => setDate(date)} format="MM/dd/yyyy" />
                    <span onClick={() => setDate(new Date())}>Now</span>
                </div>
                {!useSimpleTime ?
                    <>
                        <div className={styles.formRow}>
                            <KeyboardTimePicker label="Time Start" value={timeStart} onChange={setTimeStart} />
                            <span onClick={() => setTimeStart(new Date())}>Now</span>
                            <span className={styles.swapSpan} onClick={() => setTimeEnd(timeStart)}>↓</span>
                        </div>
                        <div className={styles.formRow}>
                            <KeyboardTimePicker label="Time End" value={timeEnd} onChange={setTimeEnd} />
                            <span onClick={() => setTimeEnd(new Date())}>Now</span>
                            <span className={styles.swapSpan} onClick={() => setTimeStart(timeEnd)}>↑</span>
                        </div>
                    </>
                    :
                    <div className={styles.formRow}>
                        <TextField label="Actual Hours" value={actualHours} onChange={e => setActualHours(e.target.value)} />
                    </div>
                }
                {
                    settings.general.experimental && 
                    <TreeSelect
                        style={{ width: '100%' }}
                        value={serviceRID}
                        dropdownStyle={{ maxHeight: 400, overflow: 'auto' }}
                        treeData={tickets}
                        placeholder="Service ID"
                        allowClear
                        treeDefaultExpandAll={false}
                        treeNodeFilterProp="search"
                        showSearch
                        onChange={val => setServiceRID(val)}
                    />
                }
                <div className={styles.formRow}>
                    <TextField label="Service ID" value={serviceRID} onChange={e => setServiceRID(e.target.value)} />
                </div>
                {notesEnabled &&
                    <div className={styles.formRow}>
                        <TextField label="Notes" value={notes} onChange={e => setNotes(e.target.value)} />
                    </div>
                }
            </div>
            <div className={styles.buttonContainer}>
                <Button
                    // Only enable if not submitting AND either api enabled or in connectwise
                    disabled={!(!submitting && (settings.auth.enableApi || (!loading && onConnectWise)))}
                    variant="contained" color="primary" fullWidth
                    onClick={() => {
                        setSubmitting(true)
                        onRun(date, timeStart, timeEnd, serviceRID, notesEnabled ? notes : "", useSimpleTime ? actualHours : null).then(() => setSubmitting(false))
                    }}
                >
                    Save
                </Button>
                {showStash && <Button variant="contained" color="secondary" fullWidth onClick={() => {
                    addStash({ date, timeStart, timeEnd, actualHours, serviceRID, notes })
                }}>Stash</Button>}
            </div>
            {!settings.auth.enableApi && !loading && !onConnectWise &&
                <h4>Please navigate to a ConnectWise Tab to save</h4>
            }
        </>
    )
}
