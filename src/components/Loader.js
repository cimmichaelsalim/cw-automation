import React from 'react'
import Loader from 'react-loader-spinner'

export default () => {
	return <Loader color="green" width={200} height={200} type="Triangle" />
}
