import { useState, useEffect } from 'react'
import { isValid, parse, parseISO } from 'date-fns'

import { useSettings } from 'providers/Settings'
import { convertDateToString } from 'helper'

var browser = require('webextension-polyfill')
const storage = browser.storage

// Transform an object of the form item to handle Dates
const transform = props => {
  return convertDateToString(props)
}

// Generic to handle stash
const defaultStorageSetter = (object) => {
  storage.local.set({ mainForm: object })
}

// Wrap everything into 1 function
const setWrapper = localStorageSetter => props => (func, val) => (data) => {
  func(data)

  const obj = transform({ ...props, [val]: data })
  localStorageSetter(obj)
}

const dataDefaults = {
  date: new Date(),
  timeStart: parse('09', 'HH', new Date()),
  timeEnd: parse('17', 'HH', new Date()),
  actualHours: '0',
  serviceRID: '',
  notes: '',
}

export default (key = 'mainForm', getter = (obj) => obj.mainForm, localStorageSetter = defaultStorageSetter, defaults = dataDefaults) => {
  const { loading, settings: { general: { notesEnabled, defaultDateToday, useSimpleTime } } } = useSettings()

  const [date, setManualDate] = useState(defaults.date)
  const [timeStart, setManualTimeStart] = useState(defaults.timeStart)
  const [timeEnd, setManualTimeEnd] = useState(defaults.timeEnd)
  const [actualHours, setManualActualHours] = useState(defaults.actualHours)
  const [serviceRID, setManualServiceRID] = useState(defaults.serviceRID)
  const [notes, setManualNotes] = useState(defaults.notes)

  // Initialize the wrap
  const wrap = setWrapper(localStorageSetter)({ date, timeStart, timeEnd, actualHours, serviceRID, notes })

  const setDate = wrap(setManualDate, 'date')
  const setTimeStart = wrap(setManualTimeStart, 'timeStart')
  const setTimeEnd = wrap(setManualTimeEnd, 'timeEnd')
  const setActualHours = wrap(setManualActualHours, 'actualHours')
  const setServiceRID = wrap(setManualServiceRID, 'serviceRID')
  const setNotes = wrap(setManualNotes, 'notes')

  // On the beginning we just load the default from last time
  useEffect(() => {
    if (!loading) {
      storage.local.get(key).then(res => {
        const formObject = getter(res)

        if (defaultDateToday) {
          setManualDate(new Date())
        } else {
          const parsedDate = parseISO(formObject.date)
          isValid(parsedDate) && setManualDate(parsedDate)
        }
        const parsedTimeStart = parseISO(formObject.timeStart)
        isValid(parsedTimeStart) && setManualTimeStart(parsedTimeStart)
        const parsedTimeEnd = parseISO(formObject.timeEnd)
        isValid(parsedTimeEnd) && setManualTimeEnd(parsedTimeEnd)

        setManualActualHours(formObject.actualHours)
        setManualServiceRID(formObject.serviceRID)
        setManualNotes(formObject.notes)
      })

    }
  }, [loading])

  return {
    date, setDate, timeStart, setTimeStart, timeEnd, setTimeEnd, serviceRID, setServiceRID, 
    notesEnabled, notes, setNotes,
    useSimpleTime, actualHours, setActualHours
  }
}