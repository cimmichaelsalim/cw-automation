import { useState, useEffect } from 'react'

var browser = require('webextension-polyfill')

const getCurrentTab = () =>
	browser.tabs
		.query({ active: true, windowId: browser.windows.WINDOW_ID_CURRENT })
		.then(tabs => browser.tabs.get(tabs[0].id))

export default () => {
	const [loading, setLoading] = useState(true)
	const [onConnectWise, setOnConnectWise] = useState(null)

	useEffect(() => {
		getCurrentTab().then(tab => {
			const url = tab.url
			if (url.indexOf('myconnectwise.net') > -1) {
				setOnConnectWise(true)
			}
			setLoading(false)
		})
	}, [])

	return { loading, onConnectWise }
}
