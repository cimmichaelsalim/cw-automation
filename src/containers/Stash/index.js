import React from 'react'
import { ExpansionPanel, ExpansionPanelSummary, ExpansionPanelDetails, Typography, IconButton } from '@material-ui/core'
import { Delete as DeleteIcon } from '@material-ui/icons'
import { format } from 'date-fns'

import { useStashes } from 'providers/Stash'
import { useGadgetStashes } from 'providers/GadgetStash'

import useStandardFormData from 'hooks/useStandardFormData'

import StandardForm from 'components/StandardForm'

import styles from './index.module.css'
import { useSettings } from 'providers/Settings'

// TODO: Address how settings are going to work
// TODO: Make sure actual hours work

export default ({ runScript }) => {
    const { settings } = useSettings()
    const { stashes } = useStashes()

    const { gadgetStashes } = useGadgetStashes()

    return (
        <div className={styles.formContainer}>
            {
                settings.general.enableGadget && 
                <>
                    <h3>Auto Stash</h3>
                    {gadgetStashes.length === 0 && <p>No stash entry.</p>}
                    {gadgetStashes.sort((a, b) => a.id - b.id).map((gadgetStash) => {
                        return (
                            <GadgetStashEntry
                                key={`gadget_stash_${gadgetStash.id}`}
                                id={gadgetStash.id}
                                onRun={(...prop) => { return runScript(...prop) }}
                            />
                        )
                    })}
                </>
            }
            <h3>Stash</h3>
            {stashes.length === 0 && <p>No stash entry.</p>}
            {stashes.sort((a, b) => a.id - b.id).map((stash) => {
                return (
                    <StashEntry
                        key={`stash_${stash.id}`}
                        id={stash.id}
                        onRun={(...prop) => { return runScript(...prop) }}
                    />
                )
            })}
        </div>
    )
}

const getter = id => (obj) => JSON.parse(obj.stashes).find(obj => obj.id === id)

const StashEntry = ({ onRun, id, key }) => {
    const { updateStash, removeStash } = useStashes()
    const {
        date, setDate, timeStart, setTimeStart, timeEnd, setTimeEnd, actualHours, setActualHours, serviceRID, setServiceRID, notesEnabled, notes, setNotes,
    } = useStandardFormData('stashes', getter(id), (object) => {
        updateStash(id, object)
    }, key)

    return (
        <ExpansionPanel>
            <ExpansionPanelSummary>
                <Typography>{format(timeStart, 'h:mma')} - {format(timeEnd, 'h:mma')} ({notes})</Typography>
            </ExpansionPanelSummary>
            <ExpansionPanelDetails>
                <div className={styles.container}>
                    <StandardForm
                        date={date}
                        setDate={setDate}
                        timeStart={timeStart}
                        setTimeStart={setTimeStart}
                        timeEnd={timeEnd}
                        setTimeEnd={setTimeEnd}
                        actualHours={actualHours}
                        setActualHours={setActualHours}
                        serviceRID={serviceRID}
                        setServiceRID={setServiceRID}
                        notesEnabled={notesEnabled}
                        notes={notes}
                        setNotes={setNotes}
                        onRun={onRun}
                        showStash={false}
                    />
                    <IconButton onClick={() => removeStash(id)} >
                        <DeleteIcon />
                    </IconButton>
                </div>
            </ExpansionPanelDetails>
        </ExpansionPanel>
    )
}


const GadgetGetter = id => (obj) => JSON.parse(obj.gadgetStashes).find(obj => obj.id === id)

const GadgetStashEntry = ({ onRun, id, key }) => {
    const { updateGadgetStash } = useGadgetStashes()
    const {
        date, setDate, timeStart, setTimeStart, timeEnd, setTimeEnd, actualHours, setActualHours, serviceRID, setServiceRID, notesEnabled, notes, setNotes,
    } = useStandardFormData('gadgetStashes', GadgetGetter(id), (object) => {
        updateGadgetStash(id, object)
    }, key)

    return (
        <ExpansionPanel>
            <ExpansionPanelSummary>
                <Typography>{format(timeStart, 'h:mma')} - {format(timeEnd, 'h:mma')}</Typography>
            </ExpansionPanelSummary>
            <ExpansionPanelDetails>
                <div className={styles.container}>
                    <StandardForm
                        date={date}
                        setDate={setDate}
                        timeStart={timeStart}
                        setTimeStart={setTimeStart}
                        timeEnd={timeEnd}
                        setTimeEnd={setTimeEnd}
                        actualHours={actualHours}
                        setActualHours={setActualHours}
                        serviceRID={serviceRID}
                        setServiceRID={setServiceRID}
                        notesEnabled={notesEnabled}
                        notes={notes}
                        setNotes={setNotes}
                        onRun={onRun}
                        showStash={false}
                    />
                </div>
            </ExpansionPanelDetails>
        </ExpansionPanel>
    )
}