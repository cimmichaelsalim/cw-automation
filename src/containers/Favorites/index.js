import React, { useState } from 'react'
import { Button, TextField, ExpansionPanel, ExpansionPanelSummary, ExpansionPanelDetails, Typography, IconButton } from '@material-ui/core'
import { Delete as DeleteIcon, AddOutlined as AddIcon } from '@material-ui/icons'

import { useFavorites } from 'providers/Favorites'

import styles from './index.module.css'

export default ({ onAdd }) => {
    const { favorites, addFavorite, removeFavorite } = useFavorites()

    const [id, setId] = useState('')
    const [name, setName] = useState('')

    return (
        <ExpansionPanel>
            <ExpansionPanelSummary>
                <Typography>Favorites</Typography>
            </ExpansionPanelSummary>
            <ExpansionPanelDetails>
                <div className={styles.formContainer}>
                    <div className={styles.formContainerHorizontal}>
                        <div className={styles.formRow}>
                            <TextField label="ID" value={id} onChange={e => setId(e.target.value)} />
                        </div>
                        <div className={styles.formRow}>
                            <TextField label="Name" value={name} onChange={e => setName(e.target.value)} />
                        </div>
                        <Button variant="contained" color="primary" onClick={() => addFavorite({ id, name })}>Add</Button>
                    </div>
                    {favorites.map((favorite, i) => {
                        return (
                            <div key={i}>
                                <IconButton onClick={() => removeFavorite(i)} >
                                    <DeleteIcon />
                                </IconButton>
                                <b>{favorite.id}</b> - {favorite.name}
                                <IconButton onClick={() => onAdd(favorite.id)}>
                                    <AddIcon />
                                </IconButton>
                            </div>
                        )
                    })}
                </div>
            </ExpansionPanelDetails>
        </ExpansionPanel>
    )
}