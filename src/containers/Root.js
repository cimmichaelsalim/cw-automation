import React, { useState } from 'react'
import { parseISO, isWithinInterval } from 'date-fns'
import { Tabs, Tab } from '@material-ui/core'
import HomeIcon from '@material-ui/icons/Home';
import AssignmentIcon from '@material-ui/icons/Assignment';
import AccountTreeIcon from '@material-ui/icons/AccountTree';
import SettingsIcon from '@material-ui/icons/Settings';
import { ToastContainer, toast } from 'react-toastify'

import getScript from 'scripts/createTimeEntry'

import Home from './Home'
import Stash from './Stash'
import Tickets from './Tickets'
import Advanced from './Advanced'

import { useSettings } from 'providers/Settings'

import { info, getTimeWithDate } from 'helper'

import styles from './Root.module.css'

export default () => {
	const { settings } = useSettings()

	const [tab, setTab] = useState("home")
	// TODO: Check if logged in

	return (
		<div className={styles.container}>
			<Tabs
				orientation="vertical"
				variant="scrollable"
				value={tab}
				onChange={(val, newVal) => setTab(newVal)}
				className={styles.verticalTab}
			>
				<Tab icon={<HomeIcon />} value="home" />
				<Tab icon={<AssignmentIcon />} value="stash" />
				{
					settings.general.experimental &&
					<Tab icon={<AccountTreeIcon />} value="tickets" />
				}
				<Tab icon={<SettingsIcon />} value="advanced" />
			</Tabs>
			<div className={styles.content}>
				{tab === "home" &&
					<Home runScript={runStandardScript(settings)} />
				}
				{tab === "stash" &&
					<Stash runScript={runStandardScript(settings)} />
				}
				{tab === "tickets" &&
					<Tickets />
				}
				{tab === "advanced" &&
					<Advanced />
				}
			</div>
			<ToastContainer
				position="top-right"
				autoClose={3000}
				hideProgressBar={false}
				newestOnTop
				closeOnClick
				rtl={false}
				pauseOnVisibilityChange
				draggable={false}
				pauseOnHover
			/>
		</div>
	)
}

const runStandardScript = ({ policy: { timeCheckEnabled, timeCheckStart, timeCheckEnd }, auth }) => async (date, timeStart, timeEnd, serviceRID, notes, actualHours) => {
	if (timeCheckEnabled) {
		const intervalToday = { start: getTimeWithDate(parseISO(timeCheckStart), new Date()), end: getTimeWithDate(parseISO(timeCheckEnd), new Date()) }

		const timeStartToday = getTimeWithDate(timeStart, new Date())
		const timeEndToday = getTimeWithDate(timeEnd, new Date())
		if (!isWithinInterval(timeStartToday, intervalToday) || !isWithinInterval(timeEndToday, intervalToday)) {
			toast.error("Policy: Time Start or Time End is outside specified time policy")
			return false
		}
	}

	try {
		const params = [
			date,
			getTimeWithDate(timeStart, date),
			getTimeWithDate(timeEnd, date),
			parseInt(serviceRID, 10),
			notes || "",
			!!actualHours ? parseFloat(actualHours) : null
		]
		info(`Calling script with parameter: ${JSON.stringify(params)}`)

		const success = await getScript(auth)(...params)

		if (success) {
			toast.success("Successfully saved time entry")
			return true
		} else {
			toast.error("Failed to save time entry")
			return false
		}

	} catch (e) {
		toast.error("Error saving time entry")
		console.error(e)
		return false
	}
}
