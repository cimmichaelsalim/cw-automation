import React from 'react'
import {
    FormControl, FormLabel, FormGroup, FormControlLabel, Switch, TextField, Button
} from '@material-ui/core'
import { KeyboardTimePicker } from '@material-ui/pickers'

import { useSettings } from 'providers/Settings'
import { useGadgetStashes } from 'providers/GadgetStash'

import styles from './index.module.css'

export default () => {
    const { updateGadgetStashes } = useGadgetStashes()
    const { settings, updateSettings } = useSettings()

    const linkSettings = (category, name) => {
        return (event) => updateSettings({ ...settings, [category]: { ...settings[category], [name]: event.target.checked } })
    }

    const linkTimeSettings = (category, name) => {
        return (value) => updateSettings({ ...settings, [category]: { ...settings[category], [name]: value } })
    }

    const linkTextSettings = (category, name) => {
        return (event) => updateSettings({ ...settings, [category]: { ...settings[category], [name]: event.target.value } })
    }

    return (
        <div className={styles.container}>
            <FormControl component="fieldset" style={{ width: "100%" }}>
                <FormLabel component="legend">General</FormLabel>
                <FormGroup>
                    <FormControlLabel
                        control={<Switch checked={settings.general.notesEnabled} onChange={linkSettings('general', 'notesEnabled')} value="notesEnabled" />}
                        label="Notes Enabled"
                    />
                    <FormControlLabel
                        control={<Switch checked={settings.general.defaultDateToday} onChange={linkSettings('general', 'defaultDateToday')} value="defaultDateToday" />}
                        label="Default Date to Today"
                    />
                    <FormControlLabel
                        control={<Switch checked={settings.general.useSimpleTime} onChange={linkSettings('general', 'useSimpleTime')} value="useSimpleTime" />}
                        label="Enable Simple Time (Input actual hours)"
                    />
                    <FormControlLabel
                        control={<Switch checked={settings.general.experimental} onChange={linkSettings('general', 'experimental')} value="experimental" />}
                        label="Try experimental features"
                    />
                    <FormControlLabel
                        control={<Switch checked={settings.general.enableGadget} onChange={linkSettings('general', 'enableGadget')} value="enableGadget" />}
                        label="Use InifinitiGadget"
                    />
                </FormGroup>

                <hr />

                <FormLabel component="legend">Policy</FormLabel>
                <FormGroup>
                    <FormControlLabel
                        control={<Switch checked={settings.policy.timeCheckEnabled} onChange={linkSettings('policy', 'timeCheckEnabled')} value="timeCheckEnabled" />}
                        label="Enable time checking"
                    />
                    <KeyboardTimePicker disabled={!settings.policy.timeCheckEnabled} label="Time Start" onChange={linkTimeSettings('policy', 'timeCheckStart')} value={settings.policy.timeCheckStart} />
                    <KeyboardTimePicker disabled={!settings.policy.timeCheckEnabled} label="Time End" onChange={linkTimeSettings('policy', 'timeCheckEnd')} value={settings.policy.timeCheckEnd} />
                </FormGroup>

                <hr />
                <FormLabel component="legend">Authentication</FormLabel>
                <FormGroup>
                    <FormControlLabel
                        control={<Switch checked={settings.auth.enableApi} onChange={linkSettings('auth', 'enableApi')} value="enableApi" />}
                        label="Use API"
                    />
                    <TextField label="Public Key" value={settings.auth.publicKey} onChange={linkTextSettings('auth', 'publicKey')} />
                    <TextField label="Private Key" value={settings.auth.privateKey} onChange={linkTextSettings('auth', 'privateKey')} />
                    <TextField label="Client ID" value={settings.auth.clientID} onChange={linkTextSettings('auth', 'clientID')} />
                </FormGroup>

                {
                    settings.general.enableGadget &&
                    <>
                        <hr />
                        <FormLabel component="legend">InfinitiGadget Orientation Mapping</FormLabel>
                        <FormGroup>
                            <TextField label="1" value={settings.orientation.o1} onChange={linkTextSettings('orientation', 'o1')} />
                            <TextField label="2" value={settings.orientation.o2} onChange={linkTextSettings('orientation', 'o2')} />
                            <TextField label="3" value={settings.orientation.o3} onChange={linkTextSettings('orientation', 'o3')} />
                            <TextField label="4" value={settings.orientation.o4} onChange={linkTextSettings('orientation', 'o4')} />
                            <TextField label="5" value={settings.orientation.o5} onChange={linkTextSettings('orientation', 'o5')} />
                            <TextField label="6" value={settings.orientation.o6} onChange={linkTextSettings('orientation', 'o6')} />
                        </FormGroup>

                        <Button onClick={() => {
                            updateGadgetStashes([])
                        }}>Reset InfinitiGadget Stash</Button>
                    </>
                }
            </FormControl>
        </div>
    )
}