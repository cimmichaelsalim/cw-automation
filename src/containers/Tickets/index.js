import React from 'react'
import { Button } from '@material-ui/core'
import { toast } from 'react-toastify'

import styles from './index.module.css'

import getProjectTree from 'scripts/projectTree'
import { useSettings } from 'providers/Settings'
import { useTickets } from 'providers/Tickets'

import { getUserName } from 'scripts/util'

export default () => {
    const { settings } = useSettings()
    const { updateTickets } = useTickets()

    return (
        <div className={styles.container}>
            <p>
                This feature is highly experimental. It probably won't work as expected. Click the button below to load the service tree.
            </p>
            <p>This requires to be run on connectwise page for now</p>
            <Button variant="contained" color="primary" onClick={() => {
                getUserName().then(userName => {
                    return getProjectTree(settings.auth)(userName)
                }).then((res) => {
                    const mappedData = res.map(data => ({ id: data.id, summary: data.summary, company: data.company, project: data.project, phase: data.phase }))

                    const groupedByPhase = mappedData.reduce((acc, val) => {
                        if (acc[val.phase.id]) {
                            acc[val.phase.id].push(val)
                        } else {
                            acc[val.phase.id] = [val]
                        }
                        return acc
                    }, {})

                    const groupedByProject = Object.values(groupedByPhase).reduce((acc, val) => {
                        if (acc[val[0].project.id]) {
                            acc[val[0].project.id].push(val)
                        } else {
                            acc[val[0].project.id] = [val]
                        }
                        return acc
                    }, {})

                    const groupedByCompany = Object.values(groupedByProject).reduce((acc, val) => {
                        if (acc[val[0][0].company.id]) {
                            acc[val[0][0].company.id].push(val)
                        } else {
                            acc[val[0][0].company.id] = [val]
                        }
                        return acc
                    }, {})

                    const result = Object.values(groupedByCompany).map(companyGroup => {
                        return {
                            title: companyGroup[0][0][0].company.name,
                            selectable: false,
                            children: companyGroup.map(projectGroup => {
                                return {
                                    title: projectGroup[0][0].project.name,
                                    selectable: false,
                                    children: projectGroup.map(phaseGroup => {
                                        return {
                                            title: phaseGroup[0].phase.name,
                                            selectable: false,
                                            children: phaseGroup.map(data => {
                                                const searchString = `${data.summary} ${phaseGroup[0].phase.name} ${projectGroup[0][0].project.name} ${companyGroup[0][0][0].company.name}`
                                                return { search: searchString, title: data.summary, value: `${data.id}`, key: data.summary }
                                            })
                                        }
                                    })
                                }
                            })
                        }
                    })

                    updateTickets(result)

                    toast.success("Successfully loaded tree data!")
                }).catch((err) => {
                    toast.error(`Failed getting tree data. Message: ${err}`)
                })
            }}>Load</Button>
        </div>
    )
}
