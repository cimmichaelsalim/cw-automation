import React from 'react'

import Favorites from 'containers/Favorites'

import StandardForm from 'components/StandardForm'

import useStandardFormData from 'hooks/useStandardFormData'

import styles from './index.module.css'

export default ({ runScript }) => {
    const {
        date, setDate, timeStart, setTimeStart, timeEnd, setTimeEnd, serviceRID, setServiceRID, 
        notesEnabled, notes, setNotes,
        useSimpleTime, actualHours, setActualHours
    } = useStandardFormData()

    return (
        <div className={styles.container}>
            <StandardForm
                date={date} setDate={setDate} timeStart={timeStart} setTimeStart={setTimeStart} timeEnd={timeEnd} setTimeEnd={setTimeEnd}
                serviceRID={serviceRID} setServiceRID={setServiceRID} 
                notesEnabled={notesEnabled} notes={notes} setNotes={setNotes}
                useSimpleTime={useSimpleTime} actualHours={actualHours} setActualHours={setActualHours}
                onRun={runScript}
            />
            <Favorites onAdd={(id => setServiceRID(id))} />
        </div>
    )
}
