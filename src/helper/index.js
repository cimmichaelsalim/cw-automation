import { isValid, parse, format } from 'date-fns'

export const info = text =>
	console.log(
		`%cInfo: %c${text}`,
		'color: blue; font-weight: bold;',
		'color: black;',
		'color: green; font-weight: bold',
	)

export const convertDateToString = object => {
	return Object.entries(object).map(([key, value]) => {
		return { [key]: value instanceof Date ? (isValid(value) ? value.toISOString() : new Date().toISOString()) : value }
	}).reduce((acc, val) => {
		return { ...acc, ...val }
	}, {})
}

export const getTimeWithDate = (time, date) => {
	return parse(format(time, 'HH:mm:ss'), 'HH:mm:ss', date)
}

export const formatCWTime = date => date.toISOString().split('.')[0] + "Z"