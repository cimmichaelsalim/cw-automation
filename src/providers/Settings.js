import React, { useContext, useState, useEffect } from 'react'
import { parse } from 'date-fns'

var browser = require('webextension-polyfill')
const storage = browser.storage

const SettingsContext = React.createContext()

export const useSettings = () => {
    return useContext(SettingsContext)
}

export const SettingsConsumer = SettingsContext.Consumer

const defaultSettings = {
    general: {
        defaultDateToday: false,
        notesEnabled: false,
        useSimpleTime: false,
        experimental: false,
        enabledGadget: false,
    },
    policy: {
        timeCheckEnabled: false,
        timeCheckStart: parse('08', 'HH', new Date()),
        timeCheckEnd: parse('19', 'HH', new Date()),
    },
    auth: {
        enableApi: false,
        publicKey: "",
        privateKey: "",
        clientID: "ab785bdd-ee7e-43fa-82c5-8cae9a3adb0b",
    },
    orientation: {
        o1: "",
        o2: "",
        o3: "",
        o4: "",
        o5: "",
        o6: "",
    }
}

export const SettingsProvider = props => {
    const [loading, setLoading] = useState(true)
    const [settings, setSettings] = useState(defaultSettings)

    const updateSettings = (newSettings) => {
        setSettings(newSettings)
        storage.local.set({ settings: JSON.stringify(newSettings) })
    }

    // Load
    useEffect(() => {
        try {
            storage.local.get('settings').then(res => {
                const loadedSettings = JSON.parse(res.settings)
                !!loadedSettings && setSettings(loadedSettings)
                setLoading(false)
            })

        } catch (e) {
            storage.local.set({ settings: JSON.stringify(defaultSettings) })
            setLoading(false)
        }
    }, [])

    return (
        <SettingsContext.Provider value={{ loading, settings, updateSettings }}>
            {props.children}
        </SettingsContext.Provider>
    )
}