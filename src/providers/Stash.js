import React, { useContext, useState, useEffect } from 'react'

var browser = require('webextension-polyfill')
const storage = browser.storage

const StashContext = React.createContext()

export const useStashes = () => {
    return useContext(StashContext)
}

export const StashConsumer = StashContext.Consumer

export const StashProvider = props => {
    // Stashes format: [{id, date, timeStart, timeEnd, serviceRID, notes}]
    const [stashes, setStashes] = useState([])

    const updateStashes = (newStashes) => {
        setStashes(newStashes)
        storage.local.set({ stashes: JSON.stringify(newStashes) })
    }

    const updateStash = (id, object) => {
        const newStash = { ...stashes.find(stash => stash.id === id), ...object }
        const stashesCopy = [...stashes.filter(stash => stash.id !== id), newStash]
        updateStashes(stashesCopy)
    }

    const addStash = (entry) => {
        // Handle objects like date
        const maxId = Math.max(...stashes.map(stash => stash.id))
        const stashesWithId = { ...entry, id: maxId + 1 }
        
        const handledEntry = JSON.parse(JSON.stringify(stashesWithId))
        updateStashes([...stashes, handledEntry])
    }
    const removeStash = (id) => updateStashes(stashes.filter(stash => stash.id !== id))

    // Load
    useEffect(() => {
        try {
            storage.local.get('stashes').then(res => {
                const loadedStashes = JSON.parse(res.stashes)
                !!loadedStashes && setStashes(loadedStashes)
            })

        } catch (e) {
            storage.local.set({ stashes: JSON.stringify([]) })
        }
    }, [])

    return (
        <StashContext.Provider value={{ stashes, updateStash, addStash, removeStash }}>
            {props.children}
        </StashContext.Provider>
    )
}