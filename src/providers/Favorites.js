import React, { useContext, useState, useEffect } from 'react'

var browser = require('webextension-polyfill')
const storage = browser.storage

const FavoritesContext = React.createContext()

export const useFavorites = () => {
    return useContext(FavoritesContext)
}

export const FavoritesConsumer = FavoritesContext.Consumer

export const FavoritesProvider = props => {
    // Favorite format: [{id, name}]
    const [favorites, setFavorites] = useState([])

    const updateFavorites = (newFavorite) => {
        setFavorites(newFavorite)
        storage.local.set({ favorites: JSON.stringify(newFavorite) })
    }

    const addFavorite = (entry) => updateFavorites([...favorites, entry])
    const removeFavorite = (index) => updateFavorites(favorites.filter((val, i) => i !== index))

    // Load
    useEffect(() => {
        try {
            storage.local.get('favorites').then(res => {
                const loadedFav = JSON.parse(res.favorites)
                !!loadedFav && setFavorites(loadedFav)
            })

        } catch (e) {
            storage.local.set({ favorites: JSON.stringify([]) })
        }
    }, [])

    return (
        <FavoritesContext.Provider value={{ favorites, updateFavorites, addFavorite, removeFavorite }}>
            {props.children}
        </FavoritesContext.Provider>
    )
}