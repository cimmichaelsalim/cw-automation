import React, { useContext, useState, useEffect } from 'react'

var browser = require('webextension-polyfill')
const storage = browser.storage

const TicketsContext = React.createContext()

export const useTickets = () => {
    return useContext(TicketsContext)
}

export const TicketsConsumer = TicketsContext.Consumer

export const TicketsProvider = props => {
    const [tickets, setTickets] = useState([])

    const updateTickets = (newTickets) => {
        setTickets(newTickets)
        storage.local.set({ tickets: JSON.stringify(newTickets) })
    }

    // Load
    useEffect(() => {
        try {
            storage.local.get('tickets').then(res => {
                const loadedTickets = JSON.parse(res.tickets)
                !!loadedTickets && setTickets(loadedTickets)
            })

        } catch (e) {
            storage.local.set({ tickets: JSON.stringify([]) })
        }
    }, [])

    return (
        <TicketsContext.Provider value={{ tickets, updateTickets }}>
            {props.children}
        </TicketsContext.Provider>
    )
}