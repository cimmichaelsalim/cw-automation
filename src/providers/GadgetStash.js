import React, { useContext, useState, useEffect } from 'react'
import axios from 'axios'
import {parseISO, isToday} from 'date-fns'
import { useSettings } from './Settings'

var browser = require('webextension-polyfill')
const storage = browser.storage

const GadgetStashContext = React.createContext()

export const useGadgetStashes = () => {
    return useContext(GadgetStashContext)
}

export const GadgetStashConsumer = GadgetStashContext.Consumer

export const GadgetStashProvider = props => {
    // Stashes format: [{id, date, timeStart, timeEnd, serviceRID, notes}]
    const {settings, loading} = useSettings()
    const [gadgetStashes, setGadgetStashes] = useState([])

    const today = new Date()

    const updateGadgetStashes = (newStashes) => {
        setGadgetStashes(newStashes)
        storage.local.set({ gadgetStashes: JSON.stringify(newStashes) })
    }

    const updateGadgetStash = (id, object) => {
        const newStash = { ...gadgetStashes.find(stash => stash.id === id), ...object }
        const stashesCopy = [...gadgetStashes.filter(stash => stash.id !== id), newStash]
        updateGadgetStashes(stashesCopy)
    }

    const addGadgetStash = (entry) => {
        // Handle objects like date
        const handledEntry = JSON.parse(JSON.stringify(entry))
        updateGadgetStashes([...gadgetStashes, handledEntry])
    }
    const removeGadgetStash = (id) => updateGadgetStashes(gadgetStashes.filter(gadgetStash => gadgetStash.id !== id))

    // Load
    useEffect(() => {
        if(!loading){
            storage.local.get('gadgetStashes').then(res => {
                let loadedStashes = []
                try {
                    loadedStashes = JSON.parse(res.gadgetStashes)
                    !!loadedStashes && setGadgetStashes(loadedStashes)
                } catch (e) {
                    console.error(e)
                    storage.local.set({ gadgetStashes: JSON.stringify([]) })
                }
    
                // Handle if today is new day already
                if(loadedStashes[0] && !isToday(parseISO(JSON.stringify(loadedStashes[0].date)))){
                    loadedStashes = []
                }
                // Then we load new data from the server
                axios('https://gadget.michaelsalim.co.uk/data').then((res) => {
                    const todayData = res.data.filter(val => {
                        const date = parseISO(val.created_at)
                        return isToday(date)
                    })
                    // We only to consider all ids except the last
                    const end = todayData.filter(val => !loadedStashes.map(dat => dat.id).includes(val.id))
                    const intervalData = end.reduce((acc, val, i, src) => {
                        if(i === src.length - 1){
                            return acc
                        }
                        
                        return [...acc, {id: val.id, date: today, timeStart: val.created_at, timeEnd: src[i + 1].created_at, serviceRID: settings.orientation[`o${val.orientation}`] || "", notes: ""}]
                    }, [])
        
                    const handledEntries = JSON.parse(JSON.stringify(intervalData))
                    updateGadgetStashes([...loadedStashes, ...handledEntries])
    
                })
            })
        }
        
    }, [loading])

    return (
        <GadgetStashContext.Provider value={{ gadgetStashes, updateGadgetStash, addGadgetStash, removeGadgetStash, updateGadgetStashes }}>
            {props.children}
        </GadgetStashContext.Provider>
    )
}