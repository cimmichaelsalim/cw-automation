# ConnectWise Automator

![Extension screenshot](media/cw_ss.png)

This is a webextension aims to help managing timesheets easier  
There are 2 method of authentication:
1. Using ConnectWise's API. This will require some keys to be obtained  
2. Using the web version of ConnectWise. This requires the extension to be opened while on a logged in page of CW. Note that the session expires after awhile and will need refreshing.

> I've been using this for the past months but use at your own risk :)

## Getting started
Get the built extension from [https://static.michaelsalim.co.uk/cw_automation.zip](https://static.michaelsalim.co.uk/cw_automation.zip). Alternatively, clone the project and build it yourself.

> Look up installing webextension for help

### Chrome
1. Go to `chrome://extensions/`
2. Enabled developers mode
3. Click on Load unpacked and select the `build` folder.

Let me know any problems in chrome as I don't actively test it there.

![Installing in chrome](media/chrome_install.gif)

### Firefox
Unless you're using the developer version, you can only load the extension temporarily(stays there until you restart)  
1. Go to `about:debugging#/runtime/this-firefox` and Load Temporary Addon.  
2. Choose `manifest.json` previously built

## Using the extension

The usage this extension is pretty simple. At its core, you simply enter the information as you would do in connectwise. Date, Time, Ticket ID and save! All this without navigating through ConnectWise.

> On the settings/advanced tab, you can toggle which columns to use such as Notes and Simple Time to avoid filling in start and end times.

Currently, you'll need to be logged in to the ConnectWise website. Open the extension on the page and it should be working. Don't worry though! This is on the road map and you'll only need to enter your credentials once in the future.

## Finding Service / Ticket ID

You can find the Service or Ticket ID from ConnectWise. Look below for example:

![Finding ticket ID](media/ticket_id.png)

This is another part of the extension that is currently being worked on. You should be able to look for the IDs right inside the extension in the near future. In fact, you can try it now if you enable the experimental flag in the settings! It probably won't work at the moment. But stay tuned!

## Favorites
As finding and remembering the ticket IDs are a big inconvenience, there is a favorites feature to store your most used tickets.

![Favorites](media/favorites.png)

You can add the ID and your prefered name as shown above. When entering the time, simply click the `+` button and it will be copied to the field!

## Stashing

The most useful feature of this extension is the power to stash your time entry for future usage. A lot of time, you don't actually know what task to associate a time with. With stash, you can fill in the details you know at the moment and keep it for the future.

This allows you to keep a record of what you did today without noting it down elsewhere. At the end of the day, it is trivial to go through them and save accordingly.

Usage of this is pretty simple. Fill in the details and then click on the `Stash` button. Navigate to the stash tab and you will see all the entry you have stashed.

![Stashing](media/stash.png)

## Obtaining API Key

1. Go to the top right dropdown and open `My Account`
2. Navigate to the `API Keys` tab
3. Create the API Keys and copy to the extension
> Note: Client ID should already be filled. In case it isn't, you can enter:   
```
ab785bdd-ee7e-43fa-82c5-8cae9a3adb0b
```
Alternatively, you can get your own through the connectwise developer page. Go [here](https://developer.connectwise.com/ClientID) for more info.

# Development
The framework of this extension is just a react app initialize using [create-react-app](https://github.com/facebook/create-react-app). As such, look at the available documentation for anything react related.

> Let `Michael` know if you're interested in contributing

### Building
1. Make sure have `NodeJS` installed. `Yarn` installation is recommended  
2. Get the packages with `npm install` or `yarn`
3. Build the project using `npm run build` or `yarn build`

> The project should be built under the *build* folder

## Roadmap
- First priority is to add an easy way to find the service ID right from the extension. This is already experimental but requires some effort to make using it smooth.
- `browser` needs to be stubbed. This will allow developing with `yarn start`, saving lots of time.
- Next is credential management. Ideally, we should use the API keys when available and fallback to using browser session. The ticket loader is currently set to be using this approach.
- Add option on where to put the time (Discussion, internal, resolution, none). This will allow the usage of this extension for service tickets.
- A bunch more! So much possibility to make life easier :)

## Known issues
- On first load, data doesn't seem to be saved. This can be resolved by changing settings.
- `yarn start` doesn't work currently due to the extension using `browser` to save data. We need to stub this eventually. Right now, development only works by doing `yarn build` every time
- Simple Time interaction with stash needs to be revisited. I think it doesn't currently work at all. The data isn't passed in.
